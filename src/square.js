import React from 'react';

export const Square = (props) => {
  let background = '';
  if (props.lineWinner) {
    if (props.lineWinner.line.includes(props.id)) {
      background = 'win' // recebe a classe win se houver um ganhador.
    }
  }
  return (
    <button id={props.id} className={`square ${background}`} onClick={props.onClick}>
      {props.value}
    </button>
  );
}