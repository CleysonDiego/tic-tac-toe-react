import { calculateWinner } from './calculateWinner'

export const ShowPlayer = ({state}) => {
    const current = state.history[state.stepNumber];
    const winner = calculateWinner(current.squares);
    let status;
    if (winner) {
        status = "Winner: " + winner.winner;
    } else if (state.stepNumber === 9) {
        status = "Draw! ";
    } else {
        status = "Next player: " + (state.xIsNext ? "X" : "O");
    }
    return status;
}