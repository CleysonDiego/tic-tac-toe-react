import React from 'react';
import { Square } from './square';

export class Board extends React.Component {
  renderSquare(i) {
    return (
      <Square
        key={i}
        id={i} // Pega cada square renderizado e seta o valor do square
        lineWinner={this.props.lineWinner}
        value={this.props.squares[i]}
        onClick={() => this.props.onClick(i)}
      />
    );
  }

  //Função responsável por criar as divs e os squares do jogo.
  createDivSquare = () => {
    let divs = [];
    let botoes = 0;
    // Loop exterior que cria o pai
    for (let i = 0; i < 3; i++) {
      let children = []
      // Loop interior que cria o filho
      for (let j = 0; j < 3; j++) {
        children.push(this.renderSquare(botoes))
        botoes = botoes + 1;
      }
      //Cria o pai e adiciona o filho
      divs.push(<div key={`${i}_linha`} className="board-row">{children}</div>)
    }
    return divs
  }

  render() {
    return (
      <div>
        {this.createDivSquare()}
      </div>
    );
  }
}