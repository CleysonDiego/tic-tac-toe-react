import React from 'react';
import { Board } from './Board';
import { ShowPlayer } from './showPlayer';
import { calculateWinner } from './calculateWinner';
import { HistoryMove } from './history';
import { Players } from './players';
import './index.css';

export class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [
        {
          squares: Array(9).fill(null)
        }
      ],
      stepNumber: 0,
      xIsNext: true,
      player1: "",
      player2: "",
    };
    this.jumpTo = this.jumpTo.bind(this)
    this.setPlayers = this.setPlayers.bind(this)
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();

    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = this.state.xIsNext ? "X" : "O";
    this.setState({
      history: history.concat([
        {
          squares: squares
        }
      ]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    });
  }

  setPlayers(input, e) {
    this.setState({
      [input]: e.target.value
    });
    console.log(this.state)
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0
    });
  }

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winner = calculateWinner(current.squares);

    return (
      <div className="game">
        <Players
          setPlayers={this.setPlayers}
        />
        <div className="game-board">
          <Board
            lineWinner={winner}
            squares={current.squares}
            onClick={i => this.handleClick(i)}
          />
        </div>
        <div className="game-info">
          <div>
            <ShowPlayer
              state={this.state}
            />
          </div>
          <ol>
            <HistoryMove
              history={this.state.history}
              jumpTo={this.jumpTo}
            />
          </ol>
        </div>
      </div>
    );
  }
}