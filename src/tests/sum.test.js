import sum, {subtract} from './sum';

test('adds 1 + 2 to equal 3', () => {
    const result = sum(1, 3);
    expect(result).toBe(3);
  });

  test('adds 1 + 2 to equal 3', () => {
    const result = sum(null, 3);
    expect(result).toBe(3);
  });

  test('adds 1 + 2 to equal 3', () => {
    const result = subtract(5, 2);
    expect(result).toBe(3);
  });