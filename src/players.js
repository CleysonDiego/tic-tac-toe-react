import React from 'react'

export const Players = ({setPlayers}) => {
    return (
        <div className="FormPlayer">
            <input onChange={(e)=> setPlayers('player1', e)} type="text" id="player1" />
            <input onChange={(e)=> setPlayers('player2', e)} type="text" id="player2" />
            <button onClick={() => alert('Inicio do jogo')}>Submit</button>
        </div>
    )
}