import React from 'react';
import PropTypes from 'prop-types';
export const HistoryMove = ({history, jumpTo}) => {
    const moves = history.map((step, move) => {
        const desc = move ?
            `Go to move # ${move}` :
            'Go to game start';
        return (
            <li key={move}>
                <button onClick={() => jumpTo(move)}>{desc}</button>
            </li>
        );
    });

    return moves;
}

HistoryMove.props = {
    history: PropTypes.array.isRequired
}